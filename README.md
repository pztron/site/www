# The code and assets for my personal website

All code and assets written by me (ie created) are licensed under AGPLv3.0, assets from other sources use their respective licenses, writing in the form of the "blog" (denoted by work in the "b" directory) is the exclusive intellectual property of Mark Muchane and is copyrighted unless otherwise noted.
